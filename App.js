import 'react-native-gesture-handler';
import * as React from 'react';
import { View, Text, Button, Dimensions } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import {
    createDrawerNavigator,
    DrawerContentScrollView,
    DrawerItemList,
    DrawerItem,
  } from '@react-navigation/drawer';

import HomeScreen from './src/screens/HomeScreen';
import AboutScreen from './src/screens/AboutScreen';
import SceneBasic from './src/components/SceneBasic';
import SceneAnimate from './src/components/SceneAnimate';
import PressBox from './src/components/gesture/PressBox';
import DraggableBox from './src/components/gesture/DraggableBox';
import CardScreen from './src/screens/CardScreen';
import Flappypod from './src/components/gameEngine/flappypod';
import BoxCollision from './src/components/gameEngine/box-collision';
import Circlepod from './src/components/gameEngine/circlepod';
import SingleTouch from './src/components/gameEngine/singleTouch';


const Drawer = createDrawerNavigator();

function CustomDrawer() {
    return (
    <Drawer.Navigator 
        initialRouteName="Circlepod" 
        drawerContent={props => CustomDrawerContent(props)} 
        drawerPosition="left"
        drawerStyle={{
            backgroundColor: "#EEE",
            padding:20,
            width: Dimensions.get('window').width,
        }}
    >   
        <Drawer.Screen name="Home" component={HomeScreen}  />
        <Drawer.Screen name="About" component={AboutScreen} />
        <Drawer.Screen name="PressBox" component={PressBox} />
        <Drawer.Screen name="Card" component={CardScreen} />
        <Drawer.Screen name="DraggableBox" component={DraggableBox} />
        <Drawer.Screen name="SceneBasic" component={SceneBasic} />
        <Drawer.Screen name="SceneAnimate" component={SceneAnimate} />
        <Drawer.Screen name="BoxCollision" component={BoxCollision} />
        <Drawer.Screen name="Flappypod" component={Flappypod} />
        <Drawer.Screen name="Circlepod" component={Circlepod} />
        <Drawer.Screen name="SingleTouch" component={SingleTouch} />
    </Drawer.Navigator>
    );
  }

  function CustomDrawerContent(props) {
    return (
      <DrawerContentScrollView {...props}>
            <DrawerItem label="Menu : Fermer" onPress={() => props.navigation.closeDrawer()} />
            <DrawerItem label="Menu : Toggle" onPress={() => props.navigation.toggleDrawer()} />
            {/*<DrawerItemList {...props} />*/}
            <View>
                <Text>Les différents écrans</Text>
                <DrawerItem label="Accueil"         onPress={() => props.navigation.navigate("Home")} />
                <DrawerItem label="Sujet"           onPress={() => props.navigation.navigate("About")} />
            </View>
            <View>
                <Text>THREE.JS</Text>
                <DrawerItem label="Scène Simple"    onPress={() => props.navigation.navigate("SceneBasic")} />
                <DrawerItem label="Scène Animée"    onPress={() => props.navigation.navigate("SceneAnimate")} />
            </View>
            <View>
                <Text>RNGE</Text>
                <DrawerItem label="Collision de box"    onPress={() => props.navigation.navigate("BoxCollision")} />
                <DrawerItem label="Jeu Flappypod"    onPress={() => props.navigation.navigate("Flappypod")} />
                <DrawerItem label="Jeu Circlepod"    onPress={() => props.navigation.navigate("Circlepod")} />
                <DrawerItem label="SingleTouch"    onPress={() => props.navigation.navigate("SingleTouch")} />
            </View>
      </DrawerContentScrollView>
    );
  }
export default function App() {
    return (
    <NavigationContainer>
            
            <View
                style={{
                    backgroundColor: "#10505b",
                    paddingTop:30,
                }}>
    
               <Button title="Toggle drawer" onPress={() => navigation.toggleDrawer()} />
            </View>
            <CustomDrawer 
                style={{
                    backgroundColor: "#10505b",
                    left:0,
                }}
            />
        </NavigationContainer>
    );
}
