import { GLView } from 'expo-gl';
import * as ExpoTHREE from 'expo-three';
import * as THREE from 'three';
import * as React from 'react';

export default function SceneBasic() {

    async function _onContextCreate (gl) {
        const { drawingBufferWidth: width, drawingBufferHeight: height } = gl;
        const sceneColor = 0x10505b;

        // Create a WebGLRenderer without a DOM element
        let renderer = new ExpoTHREE.Renderer({ gl });
        renderer.setSize(width, height);
        renderer.setClearColor(sceneColor);

        let camera = new THREE.PerspectiveCamera(80, width / height, 0.01, 1000);
        camera.position.set(2, 5, 5);

        let scene = new THREE.Scene();
        scene.add(new THREE.GridHelper(10, 10));

        renderer.render(scene, camera);
        gl.endFrameEXP();
    }

    return (
        <GLView
            style={{ flex: 1 }}
            onContextCreate={_onContextCreate}
        />
    );
}
