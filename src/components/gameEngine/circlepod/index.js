import { Dimensions, StyleSheet, StatusBar } from 'react-native';
import React, { useState } from 'react';
import Matter from "matter-js";
import { GameEngine } from "react-native-game-engine";
import  { Constants } from './Constants';
import Physics from './Physics';
import Wall from './Wall';
import Pod from './Pod';


let gameEngine = null;

/*const initGame = () => {
    return {
        physics: { engine: engine, world: world },
        initialBox: { body: initialCircle,  size: [50, 50],  color: 'red',  renderer: Circle },    
    }
}*/


// permet de calculer la taille de la boite d'un objet sur la scène
export const sizeBound = (entitie) => {
    return [entitie.bounds.max.x - entitie.bounds.min.x , entitie.bounds.max.y - entitie.bounds.min.y]
}

const TouchScreen = (entities, { touches, screen }) => {
    let world = entities["physics"].world;
    let boxSize = Math.trunc(Math.max(screen.width, screen.height) * 0.075);
    touches.filter(t => t.type === "press").forEach(t => {
            console.log(t.event);
            let body = Matter.Bodies.rectangle(
                       t.event.pageX, t.event.pageY, 
                       boxSize, boxSize,
                       { frictionAir: 0.01 });
                Matter.World.add(world, [body]);
                entities[++boxIds] = {
                    body: body,
                    size: [boxSize, boxSize],
                    color: boxIds % 2 == 0 ? "pink" : "#B8E986",
                    renderer: Box
                };
             });
    return entities;
};

const setupWorld = () => {
    let engine = Matter.Engine.create({ enableSleeping: false});
    let world = engine.world;
    world.gravity.y = 0.0;
    world.gravity.x = 0;
    //let pod = Matter.Bodies.circle(Constants.MAX_WIDTH / 2, Constants.MAX_WIDTH / 2, 50, 50);
    //let pod = Matter.Bodies.polygon(Constants.MAX_WIDTH / 2, Constants.MAX_HEIGHT / 2, 30, 20,{ 
    let pod = Matter.Bodies.circle(Constants.MAX_WIDTH / 2, Constants.MAX_HEIGHT / 2, 20,{ 
        restitution: 0.99,
        friction:0, 
        frictionAir:0,
        frictionStatic:0,
    });

    const options = {
        isStatic: true, 
        restitution: 0.99,
        friction:0, 
        frictionAir:0,
        frictionStatic:0.4,
    }
    let podUser =       Matter.Bodies.rectangle(Constants.MAX_WIDTH / 2, Constants.MAX_HEIGHT - 100, Constants.MAX_WIDTH / 2, 50, options);

    // Initialisation des murs du jeu
    let wallTop =       Matter.Bodies.rectangle(Constants.MAX_WIDTH / 2, 0, Constants.MAX_WIDTH, 100, options); 
    let wallLeft =      Matter.Bodies.rectangle(0, Constants.MAX_HEIGHT / 2, 100, Constants.MAX_HEIGHT, options);
    let wallRight =      Matter.Bodies.rectangle(Constants.MAX_WIDTH, Constants.MAX_HEIGHT / 2, 100, Constants.MAX_HEIGHT, options);

    // Ajout des différents objets sur la scène
    Matter.World.add(world, [pod, podUser, wallTop, wallLeft, wallRight]);
    Matter.Body.setVelocity(pod, {x: 3, y: 3});

   /* Matter.Events.on(engine, 'collisionEnd', (event) => {
        var pairs = event.pairs;
        /*if(pairs[0].bodyA.id == 4){
            let xV = Math.sign(pod.velocity.x) * 2;
            let yV = Math.sign(pod.velocity.y) * 2;
            Matter.Body.setVelocity(pod, {x: xV, y: yV})
            console.log(pod.velocity);
            console.log(pod.mass);
            console.log(pod.mass);
        }*/

        //console.log(Math.round(pod.velocity.x));
        //console.log(Math.round(pod.velocity.y));
       // idObj = pairs[0].bodyB.id; // Détection de l'ID de l'objet en collision
       // if(idObj == wallLeft.id) console.log('Collision à gauche');
       // if(idObj == wallRight.id) console.log('Collision à droite');

        //this.gameEngine.dispatch({ type: "game-over"});

    ///});

    return {
        physics: { engine: engine, world: world },
        pod: { body: pod, size: [20, 20], color: "#10505b", renderer: Pod},
        podUser: { body: podUser, size: sizeBound(podUser), color: "red", renderer: Wall},
        wallTop: { body: wallTop, size: sizeBound(wallTop), color: "grey", renderer: Wall },
        wallLeft: { body: wallLeft, size: sizeBound(wallLeft), color: "grey", renderer: Wall },
        wallRight: { body: wallRight, size: sizeBound(wallRight), color: "grey", renderer: Wall },
    }
}

export default function App({ navigation }) {
    return (
        <GameEngine 
        ref={(ref) => { gameEngine = ref; }}
        style={styles.container} 
        systems={[Physics]}
        entities={setupWorld()}
        >
           <StatusBar hidden={true} />
        </GameEngine>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF', //#10505b
    },
});