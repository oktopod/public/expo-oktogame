import Matter from "matter-js";
import  { Constants } from './Constants';

const Physics = (entities, { touches, time }) => {
    let engine = entities.physics.engine;
    let podUser = entities.podUser.body;

    touches.filter(t => t.type === "move").forEach(t => {
       //Matter.Body.applyForce( pod, pod.position,);
       Matter.Body.setPosition(podUser, {x: t.event.pageX, y: t.event.pageY - 50})
    });

    // Déplacement des tubes sur l'écran
    /*for(let i=1; i<=4; i++){
        if (entities["pipe" + i].body.position.x <= -1 * (Constants.PIPE_WIDTH / 2)){
            Matter.Body.setPosition( entities["pipe" + i].body, {x: Constants.MAX_WIDTH * 2 - (Constants.PIPE_WIDTH / 2), y: entities["pipe" + i].body.position.y});
        } else {
            Matter.Body.translate( entities["pipe" + i].body, {x: -2, y: 0});
        }
    }*/
    Matter.Engine.update(engine, time.delta);

    return entities;
};

export default Physics;