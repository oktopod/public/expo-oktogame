import * as React from 'react';
import { GLView } from 'expo-gl';
import * as THREE from 'three';
import * as ExpoTHREE from 'expo-three';


export default function SceneAnimate() {
    let timeout;

    React.useEffect(() => {
        // Clear the animation loop when the component unmounts
        return () => clearTimeout(timeout);
    }, []);

    async function _onContextCreate (gl) {
        const { drawingBufferWidth: width, drawingBufferHeight: height } = gl;
        const sceneColor = 0x10505b;

        // Create a WebGLRenderer without a DOM element
        let renderer = new ExpoTHREE.Renderer({ gl });
        renderer.setSize(width, height);
        renderer.setClearColor(sceneColor);

        let camera = new THREE.PerspectiveCamera(70, width / height, 0.01, 1000);
        camera.position.set(2, 5, 5);

        let scene = new THREE.Scene();
        scene.fog = new THREE.Fog(sceneColor, 1, 10000);
        scene.add(new THREE.GridHelper(10, 10));

        let ambientLight = new THREE.AmbientLight(0x101010);
        scene.add(ambientLight);

        let pointLight = new THREE.PointLight(0xffffff, 2, 1000, 1);
        pointLight.position.set(0, 200, 200);
        scene.add(pointLight);

        let spotLight = new THREE.SpotLight(0xffffff, 0.5);
        spotLight.position.set(0, 500, 100);
        spotLight.lookAt(scene.position);
        scene.add(spotLight);

        const cube = new IconMesh();
        scene.add(cube);
        camera.lookAt(cube.position);

        function update() {
            cube.rotation.y += 0.05;
            cube.rotation.x += 0.025;
        }

        // Setup an animation loop
        const render = () => {
            timeout = requestAnimationFrame(render);
            update();
            renderer.render(scene, camera);
            gl.endFrameEXP();
        };
        render();
    }

    return (
        <GLView
            style={{ flex: 1 }}
            onContextCreate={_onContextCreate}
        />
    );
}

class IconMesh extends THREE.Mesh {
    constructor() {
        super(
            new THREE.BoxBufferGeometry(1.0, 1.0, 1.0),
            new THREE.MeshStandardMaterial({
                map: new ExpoTHREE.TextureLoader().load(require('./../../assets/icon.png')),
                // color: 0xff0000
            })
        );
    }
}
