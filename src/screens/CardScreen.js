import {Button, View, StyleSheet, TouchableOpacity, Text} from "react-native";
import CardFlip from 'react-native-card-flip';

import * as React from "react";


export default function CardScreen({ navigation }) {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            {/* A finaliser bug au niveau de l'affichage
             <CardFlip style={styles.cardContainer} ref={(card) => this.card = card} >
                <TouchableOpacity style={styles.card} onPress={() => this.card.flip()} ><Text>OKTOPOD</Text></TouchableOpacity>
                <TouchableOpacity style={styles.card} onPress={() => this.card.flip()} ><Text>Démonstration</Text></TouchableOpacity>
            </CardFlip>
           */}
        </View>
    );
}

const styles = StyleSheet.create({
    cardContainer: {
      flex: 1,
      width: 100,
      height: 100,
      backgroundColor: 'plum',
    },
    scrollView: {
      flex: 1,
    },
    card: {
      width: 150,
      height: 150,
      alignSelf: 'center',
      backgroundColor: 'plum',
      margin: 10,
      zIndex: 200,
    },
  });